package com.devcamp.task56d10.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    int[] rainbows = {1, 23, 32, 43, 54, 65, 86, 10,15,16,18};


    public ArrayList<Integer> filterRequestQuery(int pos) {
        ArrayList<Integer> fillter = new ArrayList<>();
        for (int element : rainbows) {
            if (element > pos) {
                fillter.add(element);
            }
        }

        return fillter;
    }

    public int filterIndex(int index) {
        int indexs = 0;

        if (index > -1 && index < 11) {
            indexs = rainbows[index];
        }

        return indexs;
    }
}
