package com.devcamp.task56d10.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56d10.Service.RainbowService;

@RestController
@CrossOrigin
public class RainbowController {
    @Autowired
    RainbowService RainbowService;
    @GetMapping("/array-int-request-query")
    public ArrayList <Integer> getRequestQuery(@RequestParam(value  = "id", defaultValue = "0") int pos){
        return RainbowService.filterRequestQuery(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public int getIndexs(@PathVariable int index){
        return RainbowService.filterIndex(index);
    }
}
